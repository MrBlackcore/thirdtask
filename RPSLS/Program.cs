﻿using System;
using System.Text;
using System.Security.Cryptography;
using System.Linq;

namespace RPSLS
{
    class Program
    {
        static void Main(string[] playerOptions)
        {
            string playerChoice;
            string computerChoice;

            string key = keyGenerator();

            bool isEnded = false;
            while (!isEnded) {
                Console.WriteLine("HMAC:\n" + hmacKey(key));
                showOptions(playerOptions);
                if (playerOptions.Length < 3 || playerOptions.Length % 2 == 0 || !isCorrect(playerOptions)) {
                    Console.WriteLine("Error. Please follow next steps.\nThere must be at least 3 arguments. The number of arguments must be odd" +
                    "Example:\nRock Paper Scissors or Rock Paper Scissors Lizard Spock\nPlease, re-enter your arguments:");
                    string newPlayerOptions = Console.ReadLine();
                    playerOptions = newPlayerOptions.Split(' ');
                } else {
                    playerChoice = Console.ReadLine();
                    while (!isEnded) {
                        if (isCorrectMove(playerOptions, playerChoice)) {
                            computerChoice = Convert.ToString(compChoice(playerOptions));
                            Console.WriteLine("\nComputer:" + optionName(computerChoice, playerOptions) + "\nPlayer:" + optionName(playerChoice, playerOptions));
                            showWinner(playerOptions, computerChoice, playerChoice);
                            Console.WriteLine("\nHMAC key:" + hmacKey(key));
                            isEnded = true;
                        } else {
                            if (playerChoice != "0") {
                                Console.Write("Please enter correct option:");
                                playerChoice = Console.ReadLine();
                            } else {
                                Console.WriteLine("Thank you for game!");
                                isEnded = true;
                            }
                        }
                    }
                }
            }
        }

        public static bool isCorrect(string[] playerChoiceArray) {
            return playerChoiceArray.Distinct().ToArray().Length == playerChoiceArray.Length;
        }

        public static bool isCorrectMove(string[] playerChoiceArray, string move) { 
            return Convert.ToInt32(move, 10) >= 1 && Convert.ToInt32(move, 10) <= playerChoiceArray.Length;
        }

        public static string hmacKey(string key) {
            StringBuilder hmacKey = new StringBuilder();
            foreach (byte element in SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(key.ToString()))) {
                hmacKey.Append(element.ToString("x2"));
            }
            return hmacKey.ToString();
        }

        public static string keyGenerator() {
            byte[] randomThing = new byte[16];
            RandomNumberGenerator.Create().GetBytes(randomThing);
            StringBuilder newKey = new StringBuilder();
            foreach (byte element in randomThing) {
                newKey.Append(element.ToString("x2"));
            }
            return newKey.ToString();
        }

        public static string optionName(string option, string[] optionsArray) {
            string nameToReturn = optionsArray[Int16.Parse(option) - 1];
            return nameToReturn;
        }

        public static int compChoice(string[] computerChoice) {
            return new Random().Next(1, computerChoice.Length + 1);
        }

        public static void showWinner(string[] playerOptionsArray, string computerChoice, string playerChoice) {
            int computerMove = Convert.ToInt32(computerChoice, 10);
            int playerMove = Convert.ToInt32(playerChoice, 10);
            Console.WriteLine(playerMove);
            bool result = (Math.Abs(computerMove - playerMove)) <= (playerOptionsArray.Length/2);
            if (computerChoice == playerChoice) {
                Console.WriteLine("Tie!");
            } else if (result) {
                Console.WriteLine("Computer wins!");
            } else {
                Console.WriteLine("You win!");
            }  
        }

        public static void showOptions(string[] options) {
            int index = 1;
            foreach (string option in options) {
                Console.WriteLine(index + " - " + option);
                index++;
            }
            Console.WriteLine("0 - Exit");
        }

    }
}
